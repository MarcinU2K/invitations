package com.homework.invitations.Domain.Invitation;

public enum Status {
    PENDING, CONFIRMED, DECLINED
}
