package com.homework.invitations.Domain.Invitation;

public enum Action {
    CONFIRM, DECLINE
}
