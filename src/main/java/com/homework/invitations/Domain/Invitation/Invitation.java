package com.homework.invitations.Domain.Invitation;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "invitations")
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    @NotNull
    private String invitee;

    @Column
    @NotNull
    private String email;

    @Column
    private String invitationStatus; //No ENUM due to h2 limitations

    public Invitation() {
    }

    public Invitation(@NotNull String invitee, @NotNull String email, String invitationStatus) {
        this.invitee = invitee;
        this.email = email;
        this.invitationStatus = invitationStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvitee() {
        return invitee;
    }

    public void setInvitee(String invitee) {
        this.invitee = invitee;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInvitationStatus() {
        return invitationStatus;
    }

    public void setInvitationStatus(String invitationStatus) {
        this.invitationStatus = invitationStatus;
    }
}
