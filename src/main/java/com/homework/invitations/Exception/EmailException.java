package com.homework.invitations.Exception;

public class EmailException extends Exception{

    private final ErrorResponse errorResponse;

    public EmailException(ErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public EmailException(String message, Throwable cause, ErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public EmailException(String message, ErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public EmailException(Throwable cause, ErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public ErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}