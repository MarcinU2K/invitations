package com.homework.invitations.Exception;

public class ErrorResponse {

    private int code;

    private ErrorMessage error;

    private String desc;

    public ErrorResponse() {
    }

    public ErrorResponse(int code, ErrorMessage error, String desc) {
        this.code = code;
        this.error = error;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ErrorMessage getError() {
        return error;
    }

    public void setError(ErrorMessage error) {
        this.error = error;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}