package com.homework.invitations.Exception;

public class DatabaseException extends Exception{

    private final ErrorResponse errorResponse;

    public DatabaseException(ErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public DatabaseException(String message, Throwable cause, ErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public DatabaseException(String message, ErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public DatabaseException(Throwable cause, ErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public ErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}