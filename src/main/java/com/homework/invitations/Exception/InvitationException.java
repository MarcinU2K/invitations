package com.homework.invitations.Exception;

public class InvitationException extends Exception{

    private final ErrorResponse errorResponse;

    public InvitationException(ErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public InvitationException(String message, Throwable cause, ErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public InvitationException(String message, ErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public InvitationException(Throwable cause, ErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public ErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}