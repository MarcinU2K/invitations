package com.homework.invitations.Exception;

public class InviteeException extends Exception{

    private final ErrorResponse errorResponse;

    public InviteeException(ErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public InviteeException(String message, Throwable cause, ErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public InviteeException(String message, ErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public InviteeException(Throwable cause, ErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public ErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}