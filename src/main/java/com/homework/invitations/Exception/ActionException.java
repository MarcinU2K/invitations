package com.homework.invitations.Exception;

public class ActionException extends Exception{

    private final ErrorResponse errorResponse;

    public ActionException(ErrorResponse responseJson) {
        super();
        this.errorResponse = responseJson;
    }

    public ActionException(String message, Throwable cause, ErrorResponse responseJson) {
        super(message, cause);
        this.errorResponse = responseJson;
    }

    public ActionException(String message, ErrorResponse responseJson) {
        super(message);
        this.errorResponse = responseJson;
    }

    public ActionException(Throwable cause, ErrorResponse responseJson) {
        super(cause);
        this.errorResponse = responseJson;
    }

    public ErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}