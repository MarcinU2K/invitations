package com.homework.invitations.Factory;

import com.homework.invitations.Exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionFactory {

    private static final String INVITEE_EXCEPTION = "InviteeException";
    private static final String EMAIL_EXCEPTION = "EmailException";
    private static final String INVITATION_EXCEPTION = "InvitationException";
    private static final String DATABASE_EXCEPTION = "DatabaseException";
    private static final String ACTION_EXCEPTION = "ActionException";
    private final Logger log = LoggerFactory.getLogger(getClass());

    private String logMessage;

    private Integer errorCode;

    private ErrorMessage errorMessage;

    private String errorDesc;

    public ExceptionFactory() {
    }

    public ExceptionFactory(String exception, String logMessage, Integer errorCode, ErrorMessage errorMessage, String errorDesc) throws InviteeException, DatabaseException, InvitationException, EmailException, ActionException {
        this.logMessage = logMessage;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.errorDesc = errorDesc;

        switch (exception){
            case INVITEE_EXCEPTION:
                log.info(logMessage);
                throw new InviteeException(new ErrorResponse(
                        errorCode,
                        errorMessage,
                        errorDesc
                ));
            case EMAIL_EXCEPTION:
                log.info(logMessage);
                throw new EmailException(new ErrorResponse(
                        errorCode,
                        errorMessage,
                        errorDesc
                ));
            case INVITATION_EXCEPTION:
                log.info(logMessage);
                throw new InvitationException(new ErrorResponse(
                        errorCode,
                        errorMessage,
                        errorDesc
                ));
            case DATABASE_EXCEPTION:
                log.info(logMessage);
                throw new DatabaseException(new ErrorResponse(
                        errorCode,
                        errorMessage,
                        errorDesc
                ));
            case ACTION_EXCEPTION:
                log.info(logMessage);
                throw new ActionException(new ErrorResponse(
                        errorCode,
                        errorMessage,
                        errorDesc
                ));
        }
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
}