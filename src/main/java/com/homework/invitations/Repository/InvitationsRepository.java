package com.homework.invitations.Repository;

import com.homework.invitations.Domain.Invitation.Invitation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvitationsRepository extends CrudRepository<Invitation, Long> {

    Invitation findByEmail(String email);
}
