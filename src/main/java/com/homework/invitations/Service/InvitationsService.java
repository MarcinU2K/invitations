package com.homework.invitations.Service;

import com.homework.invitations.Domain.Invitation.Action;
import com.homework.invitations.Domain.Invitation.Invitation;
import com.homework.invitations.Exception.*;

public interface InvitationsService {
    Invitation createInvitation(Invitation invitation) throws InviteeException, EmailException, InvitationException, DatabaseException, ActionException;

    Iterable getAllInvitations() throws DatabaseException, InviteeException, EmailException, InvitationException, ActionException;

    Object confirmOrDeclineInvitation(String email, Action action) throws EmailException, DatabaseException, InvitationException, ActionException, InviteeException;
}
