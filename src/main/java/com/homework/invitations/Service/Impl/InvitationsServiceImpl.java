package com.homework.invitations.Service.Impl;

import com.homework.invitations.Domain.Invitation.Action;
import com.homework.invitations.Domain.Invitation.Invitation;
import com.homework.invitations.Domain.Invitation.Status;
import com.homework.invitations.Exception.*;
import com.homework.invitations.Factory.ExceptionFactory;
import com.homework.invitations.Repository.InvitationsRepository;
import com.homework.invitations.Service.InvitationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class InvitationsServiceImpl implements InvitationsService {

    private static final String PLEASE_PROVIDE_INVITEE_NAME = "Please provide invitee name";
    private static final String PLEASE_PROVIDE_EMAIL = "Please provide email";
    private static final String INVITATION_FOR = "Invitation for ";
    private static final String DOES_NOT_EXIST = " does not exist.";
    private static final String ACTION_HAS_NOT_BEEN_SPECIFIED = "Action has not been specified";
    private static final String INVITATION_HAS_NOT_BEEN_UPDATED = "Invitation has not been updated";
    private static final String NULL = "null";
    private static final String THIS_INVITATION_ALREADY_EXISTS_IN_OUR_DB = "This invitation already exists in our DB";
    private static final String SERVER_CANNOT_OBTAIN_INVITATION_LIST = "Server cannot obtain invitation list";
    private static final String INVITEE_EXCEPTION = "InviteeException";
    private static final String EMAIL_EXCEPTION = "EmailException";
    private static final String INVITATION_EXCEPTION = "InvitationException";
    private static final String DATABASE_EXCEPTION = "DatabaseException";
    private static final String ACTION_EXCEPTION = "ActionException";
    private static final String INVITEE_NOT_PROVIDED = "Invitee not provided";
    private static final String EMAIL_NOT_PROVIDED = "Email not provided";
    private static final String ALREADY_EXISTS = " already exists";
    private static final String DATABASE_ERROR = "Database error";
    private static final String INVITATION_NOT_FOUND = "Invitation not found";
    private static final String UNSPECIFIED_ACTION = "Unspecified action";
    private static final String ABOUT_TO_CREATE_NEW_INVITATION_FOR_WITH_EMAIL_ADDRESS = "About to create new invitation for {} with email address {}";
    private static final String SENDING_EMAIL_ON_TO_WITH_INVITATION_STATUS = "Sending email on {} to {} with invitation status {}";
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private InvitationsRepository invitationsRepository;

    @Override
    public Invitation createInvitation(Invitation invitation) throws InviteeException, EmailException, InvitationException, DatabaseException, ActionException {
        log.info(ABOUT_TO_CREATE_NEW_INVITATION_FOR_WITH_EMAIL_ADDRESS,
                StringUtils.isEmpty(invitation.getInvitee()) ? NULL : invitation.getInvitee(),
                StringUtils.isEmpty(invitation.getEmail()) ? NULL : invitation.getEmail());

        if(StringUtils.isEmpty(invitation.getInvitee())){
            new ExceptionFactory(INVITEE_EXCEPTION, INVITEE_NOT_PROVIDED, 400, ErrorMessage.INVITEE_NOT_PROVIDED, PLEASE_PROVIDE_INVITEE_NAME);
        }

        if(StringUtils.isEmpty(invitation.getEmail())){
            new ExceptionFactory(EMAIL_EXCEPTION, EMAIL_NOT_PROVIDED, 400, ErrorMessage.EMAIL_NOT_PROVIDED, PLEASE_PROVIDE_EMAIL);
        }

        Invitation existingInvitation = invitationsRepository.findByEmail(invitation.getEmail());

        if(existingInvitation != null){
            new ExceptionFactory(INVITATION_EXCEPTION, INVITATION_FOR + invitation.getEmail() + ALREADY_EXISTS, 400, ErrorMessage.INVITATION_EXISTS, THIS_INVITATION_ALREADY_EXISTS_IN_OUR_DB);
        }

        Invitation newInvitation = new Invitation(invitation.getInvitee(), invitation.getEmail(), Status.PENDING.toString());
        sendingConfirmationEmailOrJobTrigger(newInvitation);
        return invitationsRepository.save(newInvitation);
    }

    @Override
    public Iterable getAllInvitations() throws DatabaseException, InviteeException, EmailException, InvitationException, ActionException {
        try {
            return invitationsRepository.findAll();
        } catch (Throwable e){
            new ExceptionFactory(DATABASE_EXCEPTION, DATABASE_ERROR, 500, ErrorMessage.DATABASE_ERROR, SERVER_CANNOT_OBTAIN_INVITATION_LIST);
        }
        return null;
    }

    @Override
    public Object confirmOrDeclineInvitation(String email, Action action) throws EmailException, DatabaseException, InvitationException, ActionException, InviteeException {

        if(StringUtils.isEmpty(email)){
            new ExceptionFactory(EMAIL_EXCEPTION, EMAIL_NOT_PROVIDED, 400, ErrorMessage.EMAIL_NOT_PROVIDED, PLEASE_PROVIDE_EMAIL);
        }

        Invitation existingInvitation = invitationsRepository.findByEmail(email);

        if(existingInvitation == null){
            new ExceptionFactory(INVITATION_EXCEPTION, INVITATION_NOT_FOUND, 404, ErrorMessage.INVITATION_NOT_FOUND, INVITATION_FOR + email + DOES_NOT_EXIST);
        } else {
            if (Action.CONFIRM.equals(action)) {
                existingInvitation.setInvitationStatus(Status.CONFIRMED.toString());
            } else if (Action.DECLINE.equals(action)) {
                existingInvitation.setInvitationStatus(Status.DECLINED.toString());
            } else {
                new ExceptionFactory(ACTION_EXCEPTION, UNSPECIFIED_ACTION, 400, ErrorMessage.UNSPECIFIED_ACTION, ACTION_HAS_NOT_BEEN_SPECIFIED);
            }
        }

        try {
            invitationsRepository.save(existingInvitation);

            sendingConfirmationEmailOrJobTrigger(existingInvitation);

            return existingInvitation.getInvitationStatus();
        } catch (Throwable e){
            new ExceptionFactory(DATABASE_EXCEPTION, DATABASE_ERROR, 500, ErrorMessage.DATABASE_ERROR, INVITATION_HAS_NOT_BEEN_UPDATED);
        }
        return null;
    }

    private void sendingConfirmationEmailOrJobTrigger(Invitation existingInvitation) {
        log.info(SENDING_EMAIL_ON_TO_WITH_INVITATION_STATUS,
                StringUtils.isEmpty(existingInvitation.getEmail()) ? NULL : existingInvitation.getEmail(),
                StringUtils.isEmpty(existingInvitation.getInvitee()) ? NULL : existingInvitation.getInvitee(),
                StringUtils.isEmpty(existingInvitation.getInvitationStatus()) ? NULL : existingInvitation.getInvitationStatus());
    }
}