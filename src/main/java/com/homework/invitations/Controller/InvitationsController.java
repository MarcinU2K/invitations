package com.homework.invitations.Controller;

import com.homework.invitations.Domain.Invitation.Action;
import com.homework.invitations.Domain.Invitation.Invitation;
import com.homework.invitations.Exception.*;
import com.homework.invitations.Service.InvitationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/invitation")
public class InvitationsController {

    private static final String CONFIRM = "confirm";
    private static final String DECLINE = "decline";
    private static final String INCORRECT_ACTION_TYPE = "Incorrect action type";

    @Autowired
    private InvitationsService invitationsService;


    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity createInvitation(@RequestBody Invitation invitation){
        try{
            invitationsService.createInvitation(invitation);
            return ResponseEntity.status(HttpStatus.CREATED).body(null);
        }catch (InviteeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrorResponse());
        }catch (InvitationException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrorResponse());
        }catch (EmailException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrorResponse());
        }catch (Throwable e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllInvitations(){
        try{
            return ResponseEntity.ok(invitationsService.getAllInvitations());
        } catch (DatabaseException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getErrorResponse());
        } catch (Throwable e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PutMapping("/action/{action}/email/{email:.+}")
    public ResponseEntity confirmOrDeclineInvitation(@PathVariable String action, @PathVariable String email){
        try{
            if(StringUtils.pathEquals(action.toLowerCase(), CONFIRM)){
                return ResponseEntity.ok(invitationsService.confirmOrDeclineInvitation(email, Action.CONFIRM));
            }
            if (StringUtils.pathEquals(action.toLowerCase(), DECLINE)){
                return ResponseEntity.ok(invitationsService.confirmOrDeclineInvitation(email, Action.DECLINE));
            }
            return ResponseEntity.badRequest().body(INCORRECT_ACTION_TYPE);
        } catch (EmailException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrorResponse());
        } catch (InvitationException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getErrorResponse());
        } catch (ActionException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrorResponse());
        } catch (DatabaseException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getErrorResponse());
        } catch (Throwable e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
