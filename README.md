# Invitations

Java application exposing 4 endpoints for managing event/party invitations.

## Getting Started

To run this application simply execute run.sh (Unix) or run.bat (Windows)

### Prerequisites

Before running the scripts install:

```
- maven
```

**1. Prerequisites**

Maven is written in Java (and primarily used for building JVM programs). Thus, the major prerequisite is the Java JDK. You need to install the Java JDK (e.g. from Oracle’s download site), and you should install it to a pathname without spaces.

Once Java is installed, you must ensure that the commands from the Java JDK are in your PATH environment variable. Running, for example:

java -version
must display the right version number.

**2. Installing Maven on Windows**

To install Maven on windows, head over to Apache Maven site to download the latest version, select the Maven zip file, for example apache-maven-3.3.9-bin.zip.

Unzip it to the folder you want Maven to live.

**2.1. Adding Maven to the Environment Path**

Add both M2_HOME and MAVEN_HOME variables to the Windows environment using system properties, and point it to your Maven folder.

Update the PATH variable by appending the Maven bin folder – %M2_HOME%\bin, so that you can run the Maven’s command everywhere.

To verify it run:

mvn -version
in the command prompt. It should display the Maven version, the java version, and the operating system information. That’s it, you have set up Maven on your Windows system.

**3. Installing Maven on Linux**

To install Maven on the Linux operating system, download the latest version from the Apache Maven site, select the Maven binary tar.gz file, for example: apache-maven-3.3.9-bin.tar.gz.

Extract the archive to your desired location.

**3.1. Adding Maven to the Environment Path**

Open the command terminal and run the following commands to set the environment variables:

$ export M2_HOME=/usr/local/apache-maven/apache-maven-3.3.9
 
$ export M2=$M2_HOME/bin
 
$ export MAVEN_OPTS=-Xms256m -Xmx512m
with M2_Home path corresponding with the location of your extracted Maven files.

Now append the M2 variable to the system path:

$ export PATH=$M2:$PATH 
Finally, verify if Maven has been added by running:

$ mvn -version
The output should be as follows:

Apache Maven 3.3.3 (7994120775791599e205a5524ec3e0dfe41d4a06; 2016-12-03T17:27:37+05:30)
Maven home: /usr/local/apache-maven/apache-maven-3.3.9
 
Java version: 1.8.0_75, vendor: Oracle Corporation
 
Java home: /usr/local/java-current/jdk1.8.0_75/jre
You have successfully installed Maven on your Linux system.

**3.2. Installing Maven on Ubuntu**

In a terminal, run apt-cache search maven, to get all the available Maven packages:

$ apt-cache search maven
....
libxmlbeans-maven-plugin-java-doc - Documentation for Maven XMLBeans Plugin
maven - Java software project management and comprehension tool
maven-debian-helper - Helper tools for building Debian packages with Maven
maven2 - Java software project management and comprehension tool
The Maven package always comes with latest Apache Maven.

Run command sudo apt-get install maven, to install the latest Apache Maven.

$ sudo apt-get install maven
This will take a few minutes to download. Once downloaded you can verify by running the mvn -version to verify your installation.

**4. Installing Maven on Mac OS X**

To install Maven on Mac OS X operating system, download the latest version from the Apache Maven site, select the Maven binary tar.gz file, for example: apache-maven-3.3.9-bin.tar.gz.

Extract the archive to your desired location.

**4.1. Adding Maven to the Environment Path**

Open the terminal and switch to the directory where the files were extracted to and then login as Super – User.

Remove the tar.gz archive:

rm Downloads/apache-maven*bin.tar.gz
Fix the permissions:

chown -R root:wheel Downloads/apache-maven*
Switch the Maven contents:

mv Downloads/apache-maven* /opt/apache-maven
Archive the Admin session:

exit
Add Maven binaries to the path and append.

nano $HOME/.profile
 
export PATH=$PATH:/opt/apache-maven/bin
Ctrl+x to Save and Exit from “nano”.

To load the new set up run:

bash
To test the new installation run:

mvn -version

You are now ready to use Maven on your Mac OS X.

## Endpoints

- **(POST) /invitation** - creating new invitation

        Header: Content-Type:application/json

        Body example: { "invitee": "John Smith", "email": "john@smith.mx" }

- **(GET) /invitation** - getting full list of invitations 

- **(PUT) /invitation/action/confirm/email/some@email.com** - confirming certain invitation

- **(PUT) /invitation/action/decline/email/some@email.com** - declining certain invitation

## Built With

* [Spring](https://spring.io/) - The framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [H2](http://www.h2database.com/) - Database

## Author

* **Marcin Pisera** - [marcinu2k@gmail.com](mailto:marcinu2k@gmail.com)

## License

This project is licensed under the MIT License

## Acknowledgments

Thanks to the Evojam team for getting me even more busy! ;)
